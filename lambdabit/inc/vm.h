#ifndef __LAMBDABIT_VM_H__
#define __LAMBDABIT_VM_H__
/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "os.h"
#include "debug.h"
#include "memory.h"
#include "closure.h"
#include "object.h"
#include "values.h"
#include "types.h"

/*
 * The design of this VM ISA is inspired by:
 * <<PICOBIT: A Compact Scheme System for Microcontrollers>>
 * Authors: Vincent St-Amour and Marc Feeley

 * I've modified it for better extensibility (extra 11 instructions),
 * and it's expected not only support Scheme. -- by NalaGinrut

 TOS: top of stack

 -> small encode
 000xxxxx                      Push constant x
 001xxxxx                      Push element from ss[#x]

 -> single encode
 0100xxxx                      Push next bytecode to global offset xxxx
 0101xxxx                      Set TOS to global
 0110xxxx                      Call closure at TOS with x arguments
 0111xxxx                      Jump to closure at TOS with x arguments
 1000xxxx                      Jump to entry point at address pc + x
 1001xxxx                      Go to address pc + x if TOS is false

 -> double encoding (start from 1010)
 1010 0000 xxxxxxxx             Push constant x
 1010 0001 xxxxxxxx             Reserved
 1010 0010 xxxxxxxx             Reserved
 1010 0011 xxxxxxxx             Reserved
 1010 0100 xxxxxxxx             Reserved
 1010 0101 xxxxxxxx             Call procedure at address pc + x − 128
 1010 0110 xxxxxxxx             Jump to entry point at pc + x − 128
 1010 0111 xxxxxxxx             Go to pc + x − 128
 1010 1000 xxxxxxxx             Go to pc + x − 128 if TOS is false
 1010 1001 xxxxxxxx             Build closure with entry pc + x − 128
 1010 1110 xxxxxxxx             Push global #x
 1010 1111 xxxxxxxx             Set global #x to TOS

 -> triple encoding (start from 1011)
 1011 0000 xxxxxxxx xxxxxxxx    Call procedure at address x
 1011 0001 xxxxxxxx xxxxxxxx    Jump to entry point at address x
 1011 0010 xxxxxxxx xxxxxxxx    Go to address x
 1011 0011 xxxxxxxx xxxxxxxx    Go to address x if TOS is false
 1011 0100 xxxxxxxx xxxxxxxx    Build a closure with entry point x
 1011 0101 xxxxxxxx xxxxxxxx    Reserved
 1011 0110 xxxxxxxx xxxxxxxx    Reserved
 1011 0111 xxxxxxxx xxxxxxxx    Reserved
 1011 1000 xxxxxxxx xxxxxxxx    Reserved
 1011 1001 xxxxxxxx xxxxxxxx    Reserved
 1011 1110 xxxxxxxx xxxxxxxx    Reserved
 1011 1111 xxxxxxxx xxxxxxxx    Reserved

 -> Speical encoding
 11xxxxxx                       Primitives (+, return, get-cont, ...)
*/

#define PUSH_SMALL_CONST 0
#define LOAD_SS_SMALL 1
#define SMALL_CONST(bc) (!((bc).small_sig))
#define DOUBLE_ENCODE(bc) (0b1010 == (bc).type)
#define TRIPLE_ENCODE(bc) (0b1011 == (bc).type)
#define IS_PRIMITIVE(bc) (0b11 == (bc).small_type)

#define PUSH_GLOBAL     0b0100
#define SET_GLOBAL      0b0101
#define CALL_CLOSURE    0b0110
#define JUMP_CLOSURE    0b0111
#define JUMP            0b1000
#define JUMP_FALSE      0b1001
#define PUSH_8BIT_CONST 0b0000
#define LONG_JUMP       0b1011
#define CALL_PROC       0b0000
#define JUMP_LONG       0b0001
#define GOTO_TRUE       0b0010
#define GOTO_FALSE      0b0011
#define MAKE_CLOSURE    0b0100
#define CALL_PROC2      0b0101
#define JUMP2           0b0110
#define GOTO2_TRUE      0b0111
#define GOTO2_FALSE     0b1000
#define MAKE_CLOSURE2   0b1001
#define GLOBAL2         0b1011
#define PUSH_GLOBAL2    0b1110
#define SET_GLOBAL2     0b1111
#define PRIMITIVE       0b1111

typedef enum vm_state
  {
   VM_RUN, VM_STOP, VM_PAUSE, VM_GC
  } vm_state_t;

typedef enum encode_type
  {
   SMALL, SINGLE, DOUBLE, TRIPLE, PRIM
  } encode_t;

typedef union ByteCode8
{
  struct
  {
    unsigned small_sig: 2;
    unsigned small_type: 1;
    unsigned small_data: 5;
  };
  struct
  {
    unsigned type: 4;
    unsigned data: 4;
  };
  u8_t all;
} __packed bytecode8_t;

typedef union ByteCode16
{
  struct
  {
    unsigned bc1: 8;
    unsigned bc2: 8;
  };
  struct
  {
    unsigned _: 4;
    unsigned type: 4;
    unsigned data: 8;
  };
  u16_t all;
} __packed bytecode16_t;

typedef union ByteCode24
{
  struct
  {
    unsigned bc1: 8;
    unsigned bc2: 8;
    unsigned bc3: 8;
  };
  struct
  {
    unsigned _: 4;
    unsigned type: 4;
    unsigned data: 16;
  };
} __packed bytecode24_t;

typedef union ByteCode32
{
  struct
  {
    unsigned bc1: 8;
    unsigned bc2: 8;
    unsigned bc3: 8;
    unsigned bc4: 8;
  };
  struct
  {
    unsigned _: 4;
    unsigned type: 4;
    unsigned data: 24;
  };
  u32_t all;
} __packed bytecode32_t;

/* FIXME: I don't know the proper size, just put it here.
 *        Should be configurable later.
 */
#define VM_CODESEG_SIZE 8192
#define VM_STKSEG_SIZE 1024

typedef struct LambdaVM
{
  u32_t pc; // program counter
  u32_t sp; // stack pointer, move when objects pushed
  u32_t fp; // last frame pointer, move when env was created
  vm_state_t state;
  bytecode8_t (*fetch_next_bytecode)(struct LambdaVM*);
  bytecode8_t code[VM_CODESEG_SIZE];
  u8_t stack[VM_STKSEG_SIZE];
} __packed *vm_t;

#define FETCH_NEXT_BYTECODE()                   \
  (vm->fetch_next_bytecode(vm))

#define NEXT_DATA()                             \
  ((vm->fetch_next_bytecode(vm)).all)

#define VM_PANIC()                              \
  do{                                           \
    vm->state = VM_STOP;                        \
    printk("VM: fatal error! Panic!\n");        \
  }while(0)

#define PUSH(data)                              \
  (vm->stack[++vm->sp] = (data))

#define TOP()                                   \
  (vm->stack[vm->sp])

#define POP()                                   \
  (vm->stack[vm->sp--])

#define PUSH_FROM_SS(bc)                        \
  do{                                           \
    u8_t i = ss_read_u8(bc.data);               \
    vm->stack[++vm->sp] = i;                    \
  }while(0)

#define HANDLE_PARITY(bc)                       \
  for(int i = 0; i < bc.data; i++)              \
    {                                           \
      PUSH(NEXT_DATA());                        \
    }

/* Convention:
 * 1. Save sp to fp to restore the last frame
 * 2. Save pc to [fp] as the return address
 */
#define SAVE_ENV()                              \
  do{                                           \
    vm->fp = vm->sp;                            \
    vm->stack[vm->fp] = vm->pc;                 \
  }while(0)                                     \

void vm_init(vm_t);
void vm_run(vm_t);

#endif // End of __LAMBDABIT_VM_H__
