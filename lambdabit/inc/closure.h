#ifndef __LAMBDABIT_CLOSURE_H__
#define __LAMBDABIT_CLOSURE_H__
/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */
#include "types.h"

typedef struct Environment
{
  u8_t* stack;
  size_t size;
} __packed env_t;

typedef struct Closure
{
  u8_t entry; // the stored pc
  env_t env; // env is the stored stack frame
} __packed *closure_t;

// TODO
#endif // End of __LAMBDABIT_CLOSURE_H__
