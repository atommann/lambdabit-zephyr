#ifndef __LAMBDABIT_TYPES_H
#define __LAMBDABIT_TYPES_H
/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined LAMBDABIT_ZEPHYR
#include <zephyr/types.h>
#define bool _Bool
#else
#include "__types.h"
#endif


#ifdef __GNUC__
#ifndef __packed
#define __packed __attribute__((packed))
#endif
#endif

#endif // End of __LAMBDABIT_TYPES_H;
