#ifndef __LAMBDABIT_OBJECT_H__
#define __LAMBDABIT_OBJECT_H__
/*  Copyright (C) 2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "qlist.h"

typedef enum obj_type
  {
   integer, boolean, symbol, string, list, vector, closure, continuation
  } otype_t;

extern const u8_t true_const;
extern const u8_t false_const;
extern const u8_t null_const;

typedef union ObjectAttribute
{
  struct
  {
    unsigned type: 4;
    unsigned gc: 1;
    unsigned unique: 1;
    unsigned reserved: 2;
  };
  u8_t all;
} __packed oattr;

typedef struct Object
{
  oattr attr;
  void* value;
} __packed *object_t, object;


/* define Page List */
typedef SLIST_HEAD(ObjectListHead, ObjectHeadList) obj_list_head_t;
typedef SLIST_ENTRY(ObjectHeadList) obj_list_entry_t;

typedef struct ObjectList
{
  obj_list_entry_t obj_list;
  object_t obj;
} __packed obj_list_t;

static inline bool object_is_false(object_t obj)
{
  return (boolean == obj->attr.type) && (obj->value == (void*)&true_const);
}

static inline bool object_is_true(object_t obj)
{
  return !object_is_false(obj);
}

void init_predefined_objects(void);
#endif // End of __LAMBDABIT_OBJECT_H__
