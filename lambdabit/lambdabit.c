/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lambdabit.h"

vm_t lambdabit_init(void)
{
  vm_t vm = (vm_t)os_malloc(sizeof(struct LambdaVM));

  init_ram_heap();
  // NOTE: The allocated vm object will never be freed.
  vm_init(vm);
  return vm;
}

void lambdabit_start(void)
{
  // TODO: Print lambdabit version information
  os_printk("Welcome to LambdaBit! %s\n", CONFIG_BOARD);
  os_printk("Author: Mu Lei known as Nala Ginrut <mulei@gnu.org>\n");
  vm_t vm = lambdabit_init();
  vm_run(vm);
}
