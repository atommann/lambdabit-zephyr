/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "memory.h"

/* NOTE:
 * There're two types of stack in lambdabit:
 *   1. These are the same thing:
 *         a. dynamic stack
 *         b. runtime stack
 *         c. vm->stack
 *   2. These are another same thing:
 *         a. static stack
 *         b. compile time stack
 *         c. __static_stack
 *      Its contents are confirmed in the compile time, and stored in
 *      the lambdabit executable format (LEF). It'll be mapped to
 *      __static_stack when LEF is loaded by VM.
 */
u8_t* __static_stack = NULL;
u8_t* __global_array = NULL;

// malloc and free should work after init_ram_heap
void init_ram_heap(void)
{
#if defined LAMBDABIT_ZEPHYR
  os_printk("MM is managed by zephyr.\n");
#else
  //GLOBAL(heap) = malloc();
  os_printk("MM is in raw mode.\n");
#endif

  /* TODO:
   * 1. The static stack should be in the ROM.
   *    But our hardware haven't prepared yet, so let it be in RAM.
   * 2. The ss malloc size should be dynamic determined by LEF.
   */
  if(NULL == (__static_stack = (u8_t*)os_malloc(SS_MAX_SIZE)))
  {
    os_printk("Fatal: static stack init failed, request size: %d\n",
              SS_MAX_SIZE);
  }

  if(NULL == (__global_array = (u8_t*)os_malloc(GARR_MAX_SIZE)))
  {
    os_printk("Fatal: global array init failed, request size: %d\n",
              GARR_MAX_SIZE);
  }
}

void* raw_malloc(size_t size)
{
#if defined LAMBDABIT_BOOTSTRAP
#error "raw_malloc hasn't been implemented yet!"
#endif
  return NULL;
}

void raw_free(void* ptr)
{
#if defined LAMBDABIT_BOOTSTRAP
#error "raw_free hasn't been implemented yet!"
#endif
}
