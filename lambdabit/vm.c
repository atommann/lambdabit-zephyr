/*  Copyright (C) 2019,2020
 *        "Mu Lei" known as "NalaGinrut" <NalaGinrut@gmail.com>
 *  Lambdabit is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or  (at your option) any later version.

 *  Lambdabit is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.

 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program.
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vm.h"

static inline closure_t capture_closure(vm_t vm)
{
  return NULL;
}

// Return value should push to stack
static inline void call_closure(vm_t vm, closure_t closure)
{
  SAVE_ENV();
  os_memcpy(vm->stack, closure->env.stack, closure->env.size);
  vm->pc = closure->entry;
}

static inline void jump_closure(vm_t vm, closure_t closure)
{
  /* Jump will not save the current environment */
  os_memcpy(vm->stack, closure->env.stack, closure->env.size);
  vm->pc = closure->entry;
}

static inline void interp_small_encode(vm_t vm, bytecode8_t bc)
{
  switch(bc.small_type)
    {
    case PUSH_SMALL_CONST:
      {
        /* Push bc.data to the current stack top */
        PUSH(bc.data);
        break;
      }
    case LOAD_SS_SMALL:
      {
        /* Load small offset from static stack, 0<= offset <=31 */
        PUSH_FROM_SS(bc);
        break;
      }
    default:
      os_printk("Invalid bytecode %X\n", bc.all);
      panic("interp_small_encode panic!\n");
    }
}

static inline void interp_double_encode(vm_t vm, bytecode16_t bc)
{
  switch(bc.type)
    {
    default:
      break;
    };
}

static inline void interp_triple_encode(vm_t vm, bytecode24_t bc) {}

static inline void interp_primitive(vm_t vm, bytecode8_t bc)
{
  // TODO
}

static inline bytecode8_t fetch_next_bytecode(vm_t vm)
{
  bytecode8_t bc;

  if(vm->pc < VM_CODESEG_SIZE)
    {
      bc = vm->code[vm->pc++];
    }
  else
    {
      printk("Oops, no more bytecode!\n");
      VM_PANIC();
    }

  return bc;
}

void vm_init(vm_t vm)
{
  os_memset(vm, 0, sizeof(struct LambdaVM));
  vm->fetch_next_bytecode = fetch_next_bytecode;
  vm->state = VM_RUN;
}

static inline encode_t parse(vm_t vm, bytecode8_t bytecode)
{
  if(SMALL_CONST(bytecode))
    {
      // Small integer constant(5bit)
      return SMALL;
    }
  else if(DOUBLE_ENCODE(bytecode))
    {
      return DOUBLE;
    }
  else if(TRIPLE_ENCODE(bytecode))
    {
      return TRIPLE;
    }
  else if(IS_PRIMITIVE(bytecode))
    {
      return PRIM;
    }

  return SINGLE;
}

static inline void dispatch(vm_t vm, bytecode8_t bc)
{
  switch(parse(vm, bc))
    {
    case SMALL:
      {
        interp_small_encode(vm, bc);
        break;
      }
    case DOUBLE:
      {
        bytecode16_t bc16;
        bc16.bc1 = bc.all;
        bc16.bc2 = NEXT_DATA();
        interp_double_encode(vm, bc16);
        break;
      }
    case TRIPLE:
      {
        bytecode24_t bc24;
        bc24.bc1 = bc.all;
        bc24.bc2 = NEXT_DATA();
        bc24.bc3 = NEXT_DATA();
        interp_triple_encode(vm, bc24);
        break;
      }
    case SINGLE:
      switch(bc.type)
        {
        case PUSH_GLOBAL:
          {
            u8_t global = global_get(bc.data);
            VM_DEBUG("(global-ref %d)\n", global);
            PUSH(global);
            break;
          }
        case SET_GLOBAL:
          {
            VM_DEBUG("(global-set! %d %d)\n", bc.data, TOP());
            global_set(bc.data, POP());
            break;
          }
        case CALL_CLOSURE:
          {
            closure_t closure = (closure_t)ss_read_u32(POP());
            VM_DEBUG("(call-closure 0x%p)\n", closure);
            // bc.data is the number of arguments
            HANDLE_PARITY(bc);
            call_closure(vm, closure);
            break;
          }
        case JUMP_CLOSURE:
          {
            closure_t closure = (closure_t)ss_read_u32(POP());
            VM_DEBUG("(jump-closure 0x%p)\n", closure);
            // bc.data is the number of arguments
            HANDLE_PARITY(bc);
            jump_closure(vm, closure);
            break;
          }
        case JUMP:
          {
            VM_DEBUG("(jump 0x%x)\n", bc.data);
            // relative jump, bc.data is the offset
            vm->pc += bc.data;
            break;
          }
        case JUMP_FALSE:
          {
            object_t obj = (object_t)ss_read_u32(TOP());
            VM_DEBUG("(jump-tos-false 0x%x 0x%p)\n", bc.data, obj);
            if(object_is_false(obj))
              {
                POP();
                vm->pc += bc.data;
              }
            break;
          }
        default:
          printk("Invalid bytecode %x\n", bc.all);
        };
    case PRIM:
      interp_primitive(vm, bc);
      break;
    default:
      os_printk("Invalid bytecode type!\n");
      panic("vm_run panic!\n");
    };
}

void vm_load_compiled_file(const char *filename)
{
  printk("LambdaBit hasn't supported file loading yet!\n");
}

void vm_run(vm_t vm)
{
  while(VM_RUN == vm->state)
    {
      /* TODO:
       * 1. Add debug info
       */
      dispatch(vm, FETCH_NEXT_BYTECODE());
    }
}
