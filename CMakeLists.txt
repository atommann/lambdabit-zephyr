# SPDX-License-Identifier: LGPLv3

cmake_minimum_required(VERSION 3.13.1)

include($ENV{ZEPHYR_BASE}/cmake/app/boilerplate.cmake NO_POLICY_SCOPE)
project(lambdabit)

add_definitions(-DLAMBDABIT_ZEPHYR -DLAMBDABIT_DEBUG)
include_directories(lambdabit/inc)
target_sources(app PRIVATE
  lambdabit/lambdabit.c
  lambdabit/object.c
  lambdabit/memory.c
  lambdabit/vm.c
  src/main.c)
